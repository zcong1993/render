import { Component } from 'react'

class Test extends Component {
  componentDidMount() {
    console.log(`render test ${this.props.name} key ${this.props.k}`)
  }
  render() {
    return (
      <p className="fade-it">test {this.props.name}</p>
    )
  }
}

export default Test

import { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Test from './components/Test'

class App extends Component {
  constructor() {
    super()
    this.state = {
      arr: [1, 2, 3, 4, 5]
    }
  }
  componentDidMount() {
    setInterval(() => {
      const arr = this.state.arr.sort(() => 0.5 - Math.random())
      this.setState({ arr })
    }, 3000)
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        {this.state.arr.map((name, index) => <Test name={name} key={`${name}${index}`} k={`${name}${index}`}/>)}
        <hr />
        {this.state.arr.map((name, index) => <Test name={name} key={index} k={index}/>)}
      </div>
    )
  }
}

export default App
